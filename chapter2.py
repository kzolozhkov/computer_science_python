# Discussion +
print("start")
for i in range(0):
    print("Hello")
print("end")


# Exercise 1,2,3 +
def average():
    print("This program calculates the average of three exam scores")
    score1, score2, score3 = eval(input("Enter two scores separated by a comma: "))
    score_average = (score1 + score2 + score3) / 3
    print(score_average)


# average()


# Exercise 4 +
def main():
    print("This program converts C to Fahrenheit")
    for i in range(5):
        celsius = int(input("What is your Celsius temperature? "))
        fahrenheit = 9 / 5 * celsius + 32
        print("The temperature is", fahrenheit)


# main()

# Exercise 5 +
def temperature_table():
    print("This program prints a table of Celsius and F temperatures")

    print()
    print("input   Celsius ", " | ", "Fahrenheit")
    print("-------------------------------------------")
    celsius = 0
    for i in range(11):
        celsius = celsius + 10
        fahrenheit = 9 / 5 * celsius + 32
        print("              ", celsius, " | ", fahrenheit)


# temperature_table()


# Exercise 6,7
def futval():
    print("This program calculates the future value of a yearly investment")
    print()

    payment = eval(input("Enter amount to invest each year: "))
    apr = eval(input("Enter the annualized interest rate: "))
    years = eval(input("Enter the number of years: "))

    principal = 0.0
    for i in range(years):
        principal = (principal + payment) * (1 + apr)

    print("The amount in", years, "years is:", principal)


# futval()

# Exercise 9 +

def fahrenheit_to_c():
    print("This program converts Fahrenheit to Celsius. ")
    fahrenheit = eval(input("Enter your Fahrenheit temperature: "))
    celsius = (fahrenheit - 32) * (5 / 9)
    print(celsius)


# fahrenheit_to_c()

# Exercise 10 +

def km_to_miles():
    print("This program converts km to miles.")
    km = eval(input("Enter your KM number: "))
    miles = km * 0.62
    print(miles)


# km_to_miles()

# Exercise 11 +

def kg_to_lb():
    print("THis program convets kgs to lbs.")
    kg = eval(input("Enter your KG number: "))
    lbs = kg / 0.45
    print(int(lbs))

# kg_to_lb()

# Exercise 12 +-

def substract(x, y):
    return x / y

def add(x, y):
    return x + y

actions = ['1. Substruction', '2. Addition']

print ("This is an interactive Python calculartor." )

for i in actions:
    print(i)

for i in range(100):
    choice = input("Choose what you want to accomplish: ")
    x = eval(input("Please enter your first number: "))
    y = eval(input("Please enter your second number: "))

    if choice == '1' : print("Your result is", substract(x, y))
    if choice == '2' : print("Your result is", add(x,y))


# def calculator():
#     print("Welcome to the Interactive Python Calculator")
#     for i in range(100):
#         result = eval(input("> "))
#         print(result)
#
# calculator()